#!/usr/bin/env ruby

#
# https://github.com/michaeldv/awesome_print
#
require 'awesome_print'

#
# https://github.com/deseretbook/classy_hash
#
require 'classy_hash'
#require 'test/unit'
#require 'active_ldap'
#require 'sequel/model'

#############################################################################
#
# Interface::Mixin
#
#############################################################################
module Interface

  module Mixin
    NOT_IMPLEMENTED = 'This method is not implemented'
  end # module Mixin

end # module Interface

#############################################################################
#
# Interface::UserAdapterClassMixin
#
#############################################################################
module Interface
  ################################
  #
  #
  ################################
  module UserAdapterClassMixin

    include Interface::Mixin

    def all
      raise NOT_IMPLEMENTED
    end # def all

  end # module UserAdapterClassMixin

end # module Interface

##################
#
# Interface::UserAdapterInstanceMixin
#
##################
module Interface
  ################################
  #
  #
  ################################
  module UserAdapterInstanceMixin
    ##############################
    #
    # include code common to all interfaces
    #
    ##############################
    include Interface::Mixin

    CONNECTED = 'connected to backend'

    ##############################
    #
    # methods' interface
    #
    ##############################
    def initialize(h={})
    end
    def connect
      raise NOT_IMPLEMENTED
    end # def connect

    def disconnect
      raise NOT_IMPLEMENTED
    end # des disconnect
    def uid
      @uid
    end
  end
end
#############################################################################
#
#
#
#############################################################################
module Interface
  module BackendMixin
    BACKEND_TYPE_UNDEFINED = 'Your implementation must define the BACKEND_TYPE constant'
    def backend_type
      raise BACKEND_TYPE_UNDEFINED if BACKEND_TYPE.nil?
      BACKEND_TYPE
    end
    def connect
      raise NOT_IMPLEMENTED
    end
  end
end

#############################################################################
#
# Backend::LDAP
#
#############################################################################
module Backend
  class LDAP #< ActiveLDAP
    BACKEND_TYPE = 'LDAP Backend'

    include Interface::BackendMixin

    def connect
      puts backend_type
    end
  end
end

#############################################################################
#
# Backend::SQL
#
#############################################################################
module Backend
  class SQL #< Sequel::Model

    BACKLEND_TYPE = 'SQL Backend'

    include Interface::BackendMixin

  end
end

#############################################################################
#
#
#
#############################################################################
class UserAdapterLDAP < Backend::LDAP

  extend  Interface::UserAdapterClassMixin

  include Interface::UserAdapterInstanceMixin

  #####################
  #
  # Class methods implementation
  #
  #####################
  class << self
    def all
      [
        UserAdapterLDAP.new(uid:0),
        UserAdapterLDAP.new(uid:1),
        UserAdapterLDAP.new(uid:2)
      ]
    end
  end

  #####################
  #
  # Instance methods implementation
  #
  #####################
  def initialize(h={})
    @uid = h[:uid]
  end

  def connect
    super
    puts CONNECTED
  end
end
#############################################################################
#
#
#
#############################################################################
class UserAdapterSQL < Backend::SQL

  extend  Interface::UserAdapterClassMixin

  include Interface::UserAdapterInstanceMixin

  #####################
  #
  # Class methods implementation
  #
  #####################
  class << self
    def all
      [
        UserAdapterSQL.new(uid:0),
        UserAdapterSQL.new(uid:1),
        UserAdapterSQL.new(uid:2)
      ]
    end
  end
  #####################
  #
  # Instance methods implementation
  #
  #####################
  def initialize(h={})
    @uid = h[:uid]
  end

  def connect
    super
    puts CONNECTED
  end
end

#############################################################################
#
#
#############################################################################
class User
  #########################################
  #
  # Define class methods
  #
  #########################################
  class << self
    def all(h={})
      ap h:h
      #####################################
      #
      # Define the parameters schema
      #
      #####################################
      schema = {
        adapter: CH::G.enum(:LDAP, :SQL)
      }
      #####################################
      #
      #
      #
      #####################################
      begin
        ClassyHash.validate(h,schema)
      rescue RuntimeError => e
        puts usage
        raise e
      end

      case h[:adapter]
      when :LDAP then
        UserAdapterLDAP.all.collect do |user|
          User.new(uid:user.uid)
        end
      when :SQL  then
        UserAdapterSQL.all.collect do |user|
          User.new(uid:user.uid)
        end
      end

    end # def all

    #################################################
    #
    #
    #
    #################################################
    def usage
      [
        'Usage:',
        ' User.new(adapter: :LDAP)',
        ' User.new(adapter: :SQL)',
      ].join("\n")
    end # def usage

  end # class << self

  ###########################################################################
  #
  #
  #
  ###########################################################################
  def initialize(h={})

    #########################################################################
    #
    #
    #
    #########################################################################
    schema = {
      uid: Integer
    }

    #########################################################################
    #
    #
    #
    #########################################################################
    begin
      ClassyHash.validate(h,schema)
    rescue RuntimeError => e
      puts usage()
      raise e
     end 

    @uid = h[:uid]
  end
  ###########################################################################
  #
  #
  #
  ###########################################################################
  def usage
    [
      'Usage',
      ' User.new(uid:0)'
    ].join("\n")
  end
end

#############################################################################
#
#
#
#############################################################################
def main
  users_ldap = UserAdapterLDAP.all
  ap users_ldap:users_ldap
  user_ldap  = UserAdapterLDAP.new(uid:0)
  ap user_ldap:user_ldap

  users_sql = UserAdapterSQL.all
  ap users_sql:users_sql
  user_sql = UserAdapterSQL.new(uid:0)
  ap user_sql:user_sql

  users_ldap = User.all( :adapter => :LDAP)
  ap users_ldap:users_ldap
  users_sql = User.all(adapter: :SQL)
  ap users_sql:users_sql
end

main()
